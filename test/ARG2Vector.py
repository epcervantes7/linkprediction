#!/usr/bin/python
import csv
import sys
import os
import math
import networkx as nx

DIST_INF = sys.maxint

class ARG2Vector:
    '''

    '''
    def __init__(self):
        print ("start processing")

    def shortestPath(self,G,nodeA,nodeB):
        ''' Compute shortest path, if exist, in and graph G between nodeA and nodeB
        returns the number of
        @param G: NetworkX graph
        @param nodeA: node source
        @param nodeB: node target
        @return: number of nodes between source and target.

            If there is no path return sys.maxint
        '''
        try:
            path=nx.shortest_path(G,source=nodeA,target=nodeB)
            return(len(path)-1)
        except :
            return DIST_INF


    def AdamicAdar(self,G,nodeA,nodeB):
        '''Compute the adamicAdar index between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @return: the adamicAdar index
        '''
        commons=list(set(G.neighbors(nodeA)).intersection(set(G.neighbors(nodeB))))
        sumtotal=0.0
        for node in commons:
            neighbors=G.neighbors(node)
            sumtotal=sumtotal+1.0/math.log(float(len(neighbors)))
        return sumtotal


    def jaccardCoefficient(self,G,nodeA,nodeB):
        '''Compute the jaccard coefficient between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @return: jaccard coefficient
        '''
        intersetionNeighbors=list(set(G.neighbors(nodeA)).intersection(set(G.neighbors(nodeB))))
        unionNeighbors=list(set(G.neighbors(nodeA)).union(set(G.neighbors(nodeB))))
        try:
            print ("******************** case 1")
            jac=len(intersetionNeighbors)/float(len(unionNeighbors))
            return jac
        except :
            print ("******************** case error")
        return 0

    def sumNeighbors(self,G,nodeA,nodeB):
        '''Compute the sum of all neighbors of two nodes

        @param G:  NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @return: sum of all neighbors
        '''
        sumtotal=len(set(G.neighbors(nodeA)))+len(set(G.neighbors(nodeB)))
        return sumtotal


    def commonNeighbors(self,G,nodeA,nodeB):
        '''Compute the number of common neighbors of two nodes

        @param G:  NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @return: number of common neighbors
        '''
        commons = list(set(G.neighbors(nodeA)).intersection(set(G.neighbors(nodeB))))
        return len(commons)


    def preferentialAttachement(self,G,nodeA,nodeB):
        '''Compute the preferential attachment between two nodes

        @param G: G:  NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @return: preferential attachment
        '''

        product=len(set(G.neighbors(nodeA)))*len(set(G.neighbors(nodeB)))
        return product

#---------------------------------------------------------------------------------------------------
    def WeightedShortestPath(self,G,nodeA,nodeB,field):
        ''' Compute weighted shortest path, if exist, in and graph G between nodeA and nodeB
        returns sum of egdes' weights in the shortest path

        @param G: NetworkX graph
        @param nodeA: node source
        @param nodeB: node target
        @param field: attribute of the edges' vector to be considered
        @return: sum of egdes' weights in the shortest path.

            If there is no path return sys.maxint
        '''
        try:
            path=nx.dijkstra_path(G,source=nodeA,target=nodeB,weight=field)
            sumtotal=0
            if(len(path)>1):
                for num in range(0,len(path)-1):
                    sumtotal=sumtotal+G[path[num]][path[num+1]][field]
            else:
                sumtotal=G[nodeA][nodeB][field]
            return sumtotal
        except :
            return DIST_INF

    def WeightedAdamicAdar(self,G,nodeA,nodeB,field):
        '''Compute the weighted AdamicAdar index between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @param field: attribute of the edges' vector to be considered
        @return: weighted AdamicAdar index
        '''
        commons=list(set(G.neighbors(nodeA)).intersection(set(G.neighbors(nodeB))))
        sumTotal=0.0
        for node in commons:
            numer=G[nodeA][node][field]+G[nodeB][node][field]
            neighbors=G.neighbors(node)
            sum_=0.0
            for vizNode in neighbors:
                sum_=sum_+G[vizNode][node][field]
            sumTotal=sumTotal+numer/float(math.log(1.0+sum_))
        return sumTotal
		

    def WeightedCommonNeighbors(self,G,nodeA,nodeB,field):
        '''Compute the weighted common neighbors between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @param field: attribute of the edges' vector to be considered
        @return: weighted common neighbors
        '''
        commons=list(set(G.neighbors(nodeA)).intersection(set(G.neighbors(nodeB))))
        sumTotal=0.0
        for node in commons:
            sumTotal=sumTotal+G[nodeA][node][field]+G[nodeB][node][field]
        return sumTotal


    def WeightedSumNeighbors(self,G,nodeA,nodeB,field):
        '''Compute the weighted sum of edges to neighbors between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @param field: attribute of the edges' vector to be considered
        @return: weighted sum of edges to neighbors
        '''
        neighborsA=list(set(G.neighbors(nodeA)))
        neighborsB=list(set(G.neighbors(nodeB)))
        sumA=0.0
        sumB=0.0
        for node in neighborsA:
            sumA=sumA+G[nodeA][node][field]
        for node in neighborsB:
            sumB=sumB+G[nodeB][node][field]
        total=sumA+sumB
        return total

    def WeightedJaccardCoefficient(self,G,nodeA,nodeB,wcn,field):
        '''Compute the weighted Jaccard coefficient between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @param field: attribute of the edges' vector to be considered
        @return: weighted Jaccard coefficient
        '''
        suma=self.WeightedSumNeighbors(G,nodeA,nodeB,field)
        if suma > 0.0:
            res= wcn/float(suma)
        else:
            res=0.0
        return res

    def WeightedPreferentialAttachement(self,G,nodeA,nodeB,field):
        '''Compute the weighted preferential attachement between two nodes, nodeA and nodeB

        @param G: NetworkX graph
        @param nodeA: node A
        @param nodeB: node B
        @param field: attribute of the edges' vector to be considered
        @return: weighted preferential attachement
        '''
        neighborsA=list(set(G.neighbors(nodeA)))
        neighborsB=list(set(G.neighbors(nodeB)))
        sumA=0.0
        sumB=0.0
        for node in neighborsA:
            sumA=sumA+G[nodeA][node][field]
        for node in neighborsB:
            sumB=sumB+G[nodeB][node][field]
        produt=sumA*sumB
        return produt

		

    def createTestSet(self, folderName, t0Graph, t1Graph, field, field_inv, name):
        '''creates a file with features extracted from two graphs belonging to times t0 and t1

        @param folderName: folder where the dataset will be storage
        @param t0Graph: graph belonging to time t0
        @param t1Graph: graph belonging to time t1
        @param field: attribute of the edges' vector to be considered
        @param field_inv: inversa of attribute of the edges' vector to be considered
        @param name:
        @return:
        '''

        nodesTraining = t0Graph.nodes()
        nodesTrainingDict = dict(t0Graph.nodes(data=True))
        path =fileNameFolder+"/tests"
        try:
            os.makedirs(path)
        except OSError:
            if not os.path.isdir(path):
                raise

        trainingFile=open(path+"/"+name+".csv","w")
        for node in nodesTraining:
            for node2 in nodesTraining:
                if node < node2:

                    isOnTraining=t0Graph.has_edge(node,node2);
                    isOnTest=t1Graph.has_edge(node,node2);
                    if isOnTraining == isOnTest and isOnTraining == False:
                            #disconsidering
                            # case not existe in the past and not exist now
                            arestaId=str(node)+" "+str(node2)

                    elif isOnTraining == isOnTest and isOnTraining == True:
                            #disconsidering
                            # case existe in the past and  exist now
                            arestaId=str(node)+" "+str(node2)

                    else:
                        #all other cases
                        #case exist in the past and exist now
                        #case exist in the past and not exist now
                        #case not existe in the past and exist now

                        # publication of vertex
                        if node in nodesTrainingDict:
                            # print(nodesTrainingDict[node])
                            v1pub1=nodesTrainingDict[node]['TotArtPeriodico'] \
                                if 'TotArtPeriodico' in nodesTrainingDict[node] else 0.0
                            v1pub2=nodesTrainingDict[node]['TotArtCongresso'] \
                                if 'TotArtCongresso' in nodesTrainingDict[node] else 0.0
                            v1pub3=nodesTrainingDict[node]['TotArtInternacionais'] \
                                if 'TotArtInternacionais' in nodesTrainingDict[node] else 0.0
                        else:
                            v1pub1=v1pub2=v1pub1=0
                        if node2 in nodesTrainingDict:
                            # print(nodesTrainingDict[node2])
                            v2pub1=nodesTrainingDict[node2]['TotArtPeriodico'] \
                                if 'TotArtPeriodico' in nodesTrainingDict[node] else 0.0
                            v2pub2=nodesTrainingDict[node2]['TotArtCongresso'] \
                                if 'TotArtCongresso' in nodesTrainingDict[node] else 0.0
                            v2pub3=nodesTrainingDict[node2]['TotArtInternacionais'] \
                                if 'TotArtInternacionais' in nodesTrainingDict[node] else 0.0
                        else:
                            v2pub1=v2pub2=v2pub1=0
                        v1sum=v1pub1+v1pub2+v1pub3
                        v2sum=v2pub1+v2pub2+v2pub3
                        v1v2sum=v1sum+v2sum
                        # publication of edges
                        epub1=epub2=epub3=epub4=epub5=epub6=epub7=esum=eWsum=0
                        if(isOnTraining):
                            epub1=t0Graph[node][node2]['pub1']
                            epub2=t0Graph[node][node2]['pub2']
                            epub3=t0Graph[node][node2]['pub3']
                            epub4=t0Graph[node][node2]['pub4']
                            epub5=t0Graph[node][node2]['pub5']
                            epub6=t0Graph[node][node2]['pub6']
                            epub7=t0Graph[node][node2]['pub7']
                            esum=t0Graph[node][node2]['sumEdge']
                            eWsum=t0Graph[node][node2]['sumEdgeWeighted']
                            arestaId=str(node)+" "+str(node2)


                        swcn=self.WeightedCommonNeighbors(t0Graph,node,node2,'sumEdge')
                        swcn=round(swcn,4)
                        swsp=self.WeightedShortestPath(t0Graph,node,node2,'invSumEdge')
                        swsp=round(swsp,4)
                        swjc=self.WeightedJaccardCoefficient(t0Graph,node,node2,swcn,'sumEdge')
                        swjc=round(swjc,4)
                        swaa=self.WeightedAdamicAdar(t0Graph,node,node2,'sumEdge')
                        swaa=round(swaa,4)
                        swpa=self.WeightedPreferentialAttachement(t0Graph,node,node2,'sumEdge')
                        swpa=round(swpa,4)

                        wcn=self.WeightedCommonNeighbors(t0Graph,node,node2,'sumEdgeWeighted')
                        wcn=round(wcn,4)
                        wsp=self.WeightedShortestPath(t0Graph,node,node2,'invSumEdgeWeighted')
                        wsp=round(wsp,4)
                        wjc=self.WeightedJaccardCoefficient(t0Graph,node,node2,wcn,'sumEdgeWeighted')
                        wjc=round(wjc,4)
                        waa=self.WeightedAdamicAdar(t0Graph,node,node2,'sumEdgeWeighted')
                        waa=round(waa,4)
                        wpa=self.WeightedPreferentialAttachement(t0Graph,node,node2,'sumEdgeWeighted')
                        wpa=round(wpa,4)
                        # print("node", "node2")

                        ####################################
                        # binary
                        # classe=1
                        # if(isOnTest):
                        # 	classe=1
                        # else:
                        # 	classe=0
                        ###########################################
                        # # three classes
                        # classe=1
                        # if (isOnTest == isOnTraining and isOnTest==True ):
                        # 	classe=1
                        # elif (isOnTest == True and isOnTraining == False ):
                        # 	classe=2
                        # elif (isOnTest == False and isOnTraining == True ):
                        # 	classe=3
                        #
                        # ############################################

                        ###########################################
                        # another binary
                        #classe = 1
                        #if (isOnTest == isOnTraining and isOnTest == True):
                        #	classe = 1
                        #elif (isOnTest == True and isOnTraining == False):
                        #	classe = 0
                        #elif (isOnTest == False and isOnTraining == True):
                        #	classe = 1

                        ############################################
                        # Binary
                        classe = 1
                        # if (isOnTest == isOnTraining and isOnTest == True):
                        # 	classe = 1
                        if (isOnTest == True and isOnTraining == False):
                            classe = 0
                        elif (isOnTest == False and isOnTraining == True):
                            classe = 1


                        ############################################
                        # print("class","v1pub1", "v1pub2", "v1pub3", "v2pub1", "v2pub2",
                        # 	  "v2pub3", "v1sum","v2sum","v1v2sum",
                        # 	  "pub1", "pub2", "pub3", "pub4", "pub5", "pub6", "pub7", "esum", "eWsum",
                        # 	  "swcn","swsp","swjc","swaa","swpa","wcn","wsp","wjc","waa","wpa")
                        # print(classe,v1pub1, v1pub2, v1pub3, v2pub1, v2pub2,
                        # 	  v2pub3, v1sum,v2sum,v1v2sum,
                        # 	  epub1, epub2, epub3, epub4, epub5, epub6, epub7, esum, eWsum,
                        # 	  swcn, swsp, swjc, swaa, swpa, wcn, wsp, wjc, waa, wpa)
                        line_out=[classe,int(node),int(node2),v1pub1,v1pub2, v1pub3, v2pub1, v2pub2,
                              v2pub3, v1sum,v2sum,v1v2sum,
                              epub1, epub2, epub3, epub4, epub5, epub6, epub7, esum, eWsum,
                              swcn, swsp, swjc, swaa, swpa, wcn, wsp, wjc, waa, wpa]
                        trainingFile.write(str(line_out)+"\n")

        trainingFile.close()


    def csvToARG(self,fileName):
        '''reads csv file and creates the atribbuted relational graphs
        @param fileName: name of file to read

        '''
        currentGraph=nx.Graph()
        reader = csv.reader(open(fileName, 'r',encoding='utf8'))
        isVertex = False
        isEdge = False
        vertexHeader = []
        edgeHeader = []
        for index,row in enumerate(reader):
            if('#' in str(row)):
                if("#vertices" in row):
                    isVertex = True
                elif("#edges" in row):
                    isVertex = False
                    isEdge = True
                elif(isVertex):
                    for i in range(0,len(row)):
                        vertexHeader.append(row[i].strip()[1:])
                elif(isEdge):
                    for i in range(0,len(row)):
                        edgeHeader.append(row[i].strip()[1:])
                else:
                    sys.exit(0)
            else:
                if(isVertex):
                    currentGraph.add_node(row[0].strip())
                    for j in range(0,len(vertexHeader)):
                        if (row[j].strip().isdigit()):
                            float(row[j].strip())>0.0
                            currentGraph.node[row[0].strip()][vertexHeader[j].strip()]=float(row[j].strip())
                        else:
                            currentGraph.node[row[0].strip()][vertexHeader[j].strip()]=row[j].strip()
                if(isEdge):
                    currentGraph.add_edge(row[0].strip(),row[1].strip())

                    for j in range(0,len(edgeHeader)):
                        # print(j,edgeHeader[j].strip(),row[j].strip())
                        float(row[j].strip())
                        currentGraph.edge[row[0].strip()][row[1].strip()][edgeHeader[j].strip()]=float(row[j].strip())
                    sumPublications=self.sumPublications(row)
                    sumPublicationsWeighted=self.sumPublicationsWeighted(row)
                    if(sumPublications>0.0):
                        currentGraph.edge[row[0].strip()][row[1].strip()]['sumEdge']=sumPublications
                        currentGraph.edge[row[0].strip()][row[1].strip()]['invSumEdge']=1/sumPublications
                        currentGraph.edge[row[0].strip()][row[1].strip()]['sumEdgeWeighted']=sumPublicationsWeighted
                        currentGraph.edge[row[0].strip()][row[1].strip()]['invSumEdgeWeighted']=1/sumPublicationsWeighted
        return currentGraph
	
    def sumPublications(self, row):
        '''sums all the elements of row vector

        @param row: a vector
        @return: sums all the elements of row vector
        '''
        sumPublications=0.0
        for j in range(2,len(row)):
            sumPublications+=float(row[j])
        return sumPublications

    def sumPublicationsWeighted(self, row):
        '''weighted sums all the elements of row vector

        @param row: a vector
        @return: weighted sums all the elements of row vector
        '''
        sumPublications=0.0
        for j in range(2,len(row)):
            weight=float(len(row))-float(j)
            sumPublications=sumPublications+float(row[j])*weight
        return sumPublications

    def createTrainAndTest(self, folderName, fileNameT0, fileNameT1, fileNameT2):
        '''call to create training and test sets
        @param folderName: folder name
        @param fileNameT0: file name where the t0 data is stored
        @param fileNameT1: file name where the t1 data is stored
        @param fileNameT2: file name where the t2 data is stored
        @return:
        '''
        t0Graph=self.csvToARG(fileNameT0)
        t1Graph=self.csvToARG(fileNameT1)
        t2Graph=self.csvToARG(fileNameT2)
        self.createTestSet(folderName,t0Graph,t1Graph,'sumEdge','invSumEdge','train')
        self.createTestSet(folderName,t1Graph,t2Graph,'sumEdge','invSumEdge','test')
		#self.createTestSet(t0Graph,t1Graph,'sumEdgeWeighted','invSumEdgeWeighted','weightedTrainingSet')
		#self.createTestSet(t1Graph,t2Graph,'sumEdgeWeighted','invSumEdgeWeighted','weightedTestSet')

if __name__ == '__main__':
	folderName =sys.argv[1]
	print(folderName)
	ARG2Vector().createTrainAndTest(folderName,
                                    folderName+"/04-06/"+folderName+"-ARG.csv",
                                    folderName+"/07-09/"+folderName+"-ARG.csv",
                                    folderName+"/10-12/"+folderName+"-ARG.csv")
    
